package ru.t1.azarin.tm.api;

import ru.t1.azarin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project create(String name);

    Project create(String name, String description);

}
