package ru.t1.azarin.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
