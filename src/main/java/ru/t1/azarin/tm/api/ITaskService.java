package ru.t1.azarin.tm.api;

import ru.t1.azarin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name);

    Task create(String name, String description);

}
