package ru.t1.azarin.tm.constant;

public final class TerminalConst {

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_HELP = "help";

    public static final String CMD_INFO = "info";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_ARGUMENTS = "arguments";

    public static final String CMD_COMMANDS = "commands";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String PROJECT_LIST = "project-list";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_CLEAR = "task-clear";

    public static final String TASK_LIST = "task-list";

}
