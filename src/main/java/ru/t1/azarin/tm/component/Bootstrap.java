package ru.t1.azarin.tm.component;

import ru.t1.azarin.tm.api.*;
import ru.t1.azarin.tm.constant.ArgumentConst;
import ru.t1.azarin.tm.constant.TerminalConst;
import ru.t1.azarin.tm.controller.CommandController;
import ru.t1.azarin.tm.controller.ProjectController;
import ru.t1.azarin.tm.controller.TaskController;
import ru.t1.azarin.tm.repository.CommandRepository;
import ru.t1.azarin.tm.repository.ProjectRepository;
import ru.t1.azarin.tm.repository.TaskRepository;
import ru.t1.azarin.tm.service.CommandService;
import ru.t1.azarin.tm.service.ProjectService;
import ru.t1.azarin.tm.service.TaskService;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        switch (param) {
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showInfo();
                break;
            default:
                commandController.showErrorArg();
        }
        return true;
    }

    private void processCommands(final String command) {
        if (command == null || command.isEmpty()) {
            commandController.showErrorCmd();
            return;
        }
        switch (command) {
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
            default:
                commandController.showErrorCmd();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    public void run(final String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }
        System.out.println("** WELCOME TO TASK-MANAGER **");
        while (true) {
            System.out.println("ENTER THE COMMAND:");
            String command = TerminalUtil.nextLine();
            processCommands(command);
        }
    }

}
