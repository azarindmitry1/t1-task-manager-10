package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.ICommandRepository;
import ru.t1.azarin.tm.constant.ArgumentConst;
import ru.t1.azarin.tm.constant.TerminalConst;
import ru.t1.azarin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display application version."
    );

    private static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    private static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display application command."
    );

    private static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display process and memory info."
    );

    private static Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS,
            "Display arguments of application."
    );

    private static Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS,
            "Display commands of application."
    );

    private static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Clear all projects."
    );

    private static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show all projects."
    );

    private static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    private static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Clear all tasks."
    );

    private static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show all tasks."
    );

    private static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            VERSION, ABOUT, HELP, INFO, ARGUMENTS, COMMANDS, PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            TASK_CREATE, TASK_CLEAR, TASK_LIST, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}


